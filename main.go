package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB
var err error

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "12345679"
	dbname   = "db1"
)

type Person struct {
	ID        uint
	FirstName string
	LastName  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

// Delete
func DeletePerson(c *gin.Context) {
	id := c.Params.ByName("id")
	var person Person
	d := db.Where("id = ?", id).Delete(&person)
	fmt.Println(d)
	c.JSON(200, gin.H{"id #" + id: "deleted"})
}

// Update
func UpdatePerson(c *gin.Context) {

	var person Person
	id := c.Params.ByName("id")

	if err := db.Where("id = ?", id).First(&person).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	}
	c.BindJSON(&person)

	db.Save(&person)
	c.JSON(200, person)

}

// Create
func CreatePerson(c *gin.Context) {

	var person Person
	c.BindJSON(&person)
	// fmt.Printf(" person: %v\n", person)

	db.Create(&person)
	c.JSON(200, person)
}

// Get by name
func GetPersonByName(c *gin.Context) {
	name := c.Params.ByName("name")
	var person Person
	if err := db.Where("first_name = ?", name).Find(&person).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, person)
	}

}

// Get by id
func GetPerson(c *gin.Context) {
	id := c.Params.ByName("id")
	var person Person
	if err := db.Where("id = ?", id).First(&person).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, person)
	}
}

// Get all
func GetPeople(c *gin.Context) {
	var people []Person
	if err := db.Find(&people).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, people)
	}

}

func main() {

	// NOTE: See we're using = to assign the global var
	//         	instead of := which would assign it only in this function
	// db, err = gorm.Open("sqlite3", "./gorm.db")

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err = gorm.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println(err)
	}
	// defer db.Close()

	check_table := db.HasTable("people")
	fmt.Printf(strconv.FormatBool(check_table))

	db.AutoMigrate(&Person{})

	r := gin.Default()
	r.GET("/people/", GetPeople)
	r.GET("/name/:name", GetPersonByName)
	r.GET("/people/:id", GetPerson)
	r.POST("/people", CreatePerson)
	r.PUT("/people/:id", UpdatePerson)
	r.DELETE("/people/:id", DeletePerson)

	r.Run(":8080")
}
